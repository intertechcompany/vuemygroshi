import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter);
//Require authorization:
//   {
//     path: '/rout',
//     name: 'ComponentName',
//     meta: {
//        requiresAuth: true
//     },
//     component: () => import('../views/ComponentName.vue')
//   },
//
  const routes = [
    {
      path: '/',
      name: 'Home',
      component: () => import('../views/Home.vue')
    },
    {
      path: '/documents',
      name: 'Documents',
      component: () => import('../views/Documents.vue')
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: () => import('../views/Contacts.vue')
    },
    {
      path: '/feedback',
      name: 'Feedback',
      component: () => import('../views/Feedback.vue')
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('../views/Login.vue')
    },
    {
      path: '/profile',
      name: 'Profile',
      component: () => import('../views/Profile.vue'),
      meta: { title: 'Profile', requiresAuth: true },
    },
    {
      path: '/update/password',
      name: 'ChangePassword',
      component: () => import('../views/PasswordUpdate.vue'),
      meta: {
        requiresAuth: true
      },
    },
    {
      path: '/registration',
      name: 'Registration',
      component: () => import('../views/Registration.vue')
    },
    {
      path: '/logout',
      name: 'Logout',
      component: () => import('../components/Logout.vue'),
    },
    // {
    //   path: '/promocodes',
    //   name: 'Promocodes',
    //   meta: { title: 'Promo codes', requiresAuth: true },
    //   component: () => import('../views/Promocodes.vue'),
    // },
    {
      path: '/premium',
      name: 'Premium',
      meta: { title: 'Premium' },
      component: () => import('../views/Premium.vue')
    },
    {
      path: '/microloan',
      name: 'Microloan',
      meta: {type: 'microloan'},
      component: () => import('../views/Microloan.vue'),
      children: [
        {
          path: 'cashloan',
          name: 'Cashloan',
          meta: {type: 'cashloan'},
          component: () => import('../views/Microloan.vue')
        },
        {
          path: 'creditcard',
          name: 'Creditcard',
          meta: {type: 'creditcard'},
          component: () => import('../views/Microloan.vue')
        },
      ]
    },
    {
      path: '/recovery',
      name: 'Recovery',
      component: () => import('../views/Recovery.vue')
    },
    {
      path: '/unsubscribe',
      name: 'Unsubscribe',
      component: () => import('../views/Unsubscribe.vue')
    },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});


// Check Route Access
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters['auth/isLoggedIn']) {
      next()
      return;
    }
    next('/login')
  } else {
    next()
  }
})

export default router;
