import Vue from 'vue'
import App from './App.vue'
import VScrollLock from 'v-scroll-lock'
import vClickOutside from 'v-click-outside'
import VueTheMask from 'vue-the-mask'
import Vuelidate from 'vuelidate'
import VueSimpleAlert from 'vue-simple-alert';
import browserDetect from 'vue-browser-detect-plugin';

import './registerServiceWorker'

import router from './router'
import store from './store'
import axios from 'axios'
import api from './api'
import './ml'
import * as dateUtils from './utils/DateUtils';

import './assets/style/main.scss'

Vue.config.productionTip = false;

const token = store.getters['auth/token'];
if (token) {
  axios.defaults.headers.common['Accept'] = 'application/json';
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}

Vue.prototype.$axios = axios;
Vue.prototype.$api = api;
Vue.prototype.$dateUtils = dateUtils;

//Logout if token expired
axios.interceptors.response.use(undefined, (error) => {
  if (error.response && error.response.status === 401) {
    store.dispatch('auth/logout').then(() => {
      router.push({name: 'Login'});
    });
  }
  return Promise.reject(error.response.data);
});


Vue.filter('plural', function (value, forms) {
  return forms[(value % 10 === 1 && value % 100 !== 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2)];
});

Vue.filter('divide', function (value) {
  let int = String(Math.trunc(value));
  if (int.length <= 3) return int;
  let space = 0;
  let number = '';
  for (let i = int.length - 1; i >= 0; i--) {
    if (space === 3) {
      number = ' ' + number;
      space = 0;
    }
    number = int.charAt(i) + number;
    space++;
  }
  return number;
});

Vue.filter('date', function (date) {
  let newDate = new Date(date);
  return newDate.toLocaleDateString("ru-RU");
});

Vue.use(VScrollLock);
Vue.use(vClickOutside);
Vue.use(VueTheMask);
Vue.use(Vuelidate);
Vue.use(browserDetect);
Vue.use(VueSimpleAlert);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
