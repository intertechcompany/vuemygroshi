import Vue from 'vue'
import Vuex from 'vuex'

import VuexPersist from 'vuex-persist'
import auth from './modules/auth';
import recovery from './modules/recovery';
import offers from './modules/offers';
import registration from './modules/registration';
import feedback from './modules/feedback';
import promocode from './modules/promocode';
import profile from './modules/profile';
import settings from './modules/settings';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const vuexPersist = new VuexPersist({
  key: 'MyGroshi',
  storage: window.localStorage
});

export default new Vuex.Store({
  modules: {
    auth,
    offers,
    feedback,
    registration,
    promocode,
    profile,
    recovery,
    settings
  },
  strict: debug,
  plugins: [vuexPersist.plugin]
});