import api from "../../api";
import axios from "axios";

const state = {

};

const getters = {
  user: state => state.user
};

const actions = {
  update({commit}, data) {
    if (data.hasOwnProperty('field')) {
      if ('name' === data.field) {
        data.data = data.data.split(' ');
      }
      if ('phone' === data.field) {
        data.data = parseInt(data.data.replace(/\D+/g, ''));
      }
      if ('birthday' === data.field) {
        let birthday = new Date(data.data);
        data.data = birthday.toLocaleDateString();
      }
    }
    let payload = {};
    if ('name' === data.field) {
      payload = {
        last_name: data.data[0],
        first_name: data.data[1],
        middle_name: data.data[2],
      };
    } else {
      payload = {
        [data.field]: data.data,
      };
    }

    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.update, data: payload, method: 'PUT'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },

  addCard({commit}) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.card.add, method: 'POST'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },

  deleteCard({commit}, data) {
    if(data === null) {
      return;
    }

    const card = {
      card_number: data.replace(/X/g, ''),
    };

    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.card.remove, data: card, method: 'DELETE'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },

  premiumStart({state, commit}) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.premium.start, method: 'POST'})
        .then(resp => {
          if (resp.data.url) {
            window.location.href = resp.data.url;
          }
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },

  premiumStop({state, commit}) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.premium.stop, method: 'POST'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
};

const mutations = {};

export default {
  namespaced: true,
  state, getters, actions, mutations
};
