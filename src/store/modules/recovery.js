import api from "../../api";
import axios from "axios";
import {findVal} from "@/utils/ObjectHelper";

const state = {

};

const getters = {
  token: state => state.token
};

const actions = {
  sendCode({commit}, phone) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.phone.send, data: { phone: phone, type: 'sms' } , method: 'POST'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },

  checkCode({commit}, info) {
    const data = {
      phone: info.phone,
      code: info.code,
    };

    console.log(data);

    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.phone.check, data, method: 'POST'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
};

const mutations = {};

export default {
  namespaced: true,
  state, getters, actions, mutations
};