import api from "../../api";
import axios from "axios";

const state = {
  settings: {},
};

const getters = {
  settings: state => state.settings
};

const actions = {
  getSettings({commit}) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.settings, method: 'GET'})
        .then(resp => {
          if(resp.data.success) {
            commit('saveSettings', resp.data.data)
          }
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
};

const mutations = {
  saveSettings(state, data) {
    state.settings = data
  },
};

export default {
  namespaced: true,
  state, getters, actions, mutations
};