import Vue from 'vue';
import api from "../../api";
import axios from "axios";
import {findVal} from "../../utils/ObjectHelper";

const state = () => ({
  isStarted: false,
  currentStep: 'confirmPhone',
  steps: [
    {
      index: 1,
      key: 'confirmPhone',
    },
    {
      index: 2,
      key: 'first',
      number: 1
    },
    {
      index: 3,
      key: 'second',
      number: 2,
    },
    {
      index: 4,
      key: 'third',
      number: 3,
    },
    // {
    //     index: 5,
    //     key: 'confirmMailFinish'
    // },
    {
      index: 5,
      key: 'checkData'
    },
    {
      index: 6,
      key: 'checkDataFinish'
    },
    {
      index: 7,
      key: 'addCard'
    },
    // {
    //     index: 9,
    //     key: 'addCardFinish'
    // },
    // {
    //     index: 10,
    //     key: 'addCardConfirm'
    // },
  ],
  creditPurpose: {},
  workPeriod: {},
  cities: {},
  employments: {},
  data: {},
  progressValue: {
    lastName: 8,
    firstName: 8,
    middleName: 4,
    gender: 2,
    birthday: 10,
    city: 8,
    email: 12,
    employment: 12,
    workPeriod: 4,
    creditCard: 17,
    confirmEmail: 15,
  },
  totalCheck: 0,
  amount: 1000,
});

const getters = {
  amount: state => {
    return state.amount;
  },
  currentStep: state => {
    return state.currentStep;
  },
  languageItems: state => {
    return state.languageItems;
  },
  stepData: state => {
    return state.data[state.currentStep];
  },
  steps: state => {
    return state.steps;
  },
  stepByName: state => stepName => {
    return state.steps.find(step => step.key === stepName);
  },
  stepByIndex: state => index => {
    return state.steps.find(step => step.index === index);
  },
  workPeriod: state => {
    return state.workPeriod;
  },
  creditPurpose: state => {
    return state.creditPurpose;
  },
  cities: state => {
    return state.cities;
  },
  employments: state => {
    return state.employments;
  },
  email: state => {
    return findVal(state.data, 'email');
  },
  phone: state => {
    return findVal(state.data, 'phone');
  },
  name: state => {
    return `${findVal(state.data, 'firstName') || ''} ${findVal(state.data, 'middleName') || ''}`;
  },
  progress: state => {
    let progress = 0;
    for (let item in state.progressValue) {
      if (findVal(state.data, item)) {
        progress += state.progressValue[item];
      }
    }

    return progress;
  },
  checkDataItems() {
    return [
      'registration.checkDataStep.items.name',
      'registration.checkDataStep.items.birthday',
      'registration.checkDataStep.items.work',
      'registration.checkDataStep.items.email',
      'registration.checkDataStep.items.phone',

    ];
  },
  totalCheck: state => state.totalCheck,
  isStarted: state => state.isStarted,
};

const actions = {
  setRegistrationStatus({commit}, isStarted) {
    commit('setRegistrationStatus', isStarted)
  },
  setCurrentStep({commit}, step) {
    commit('setCurrentStep', step)
  },
  setAmount({commit}, amount) {
    commit('setAmount', amount)
  },
  saveData({commit}, data) {
    commit('saveData', data)
  },
  saveField({commit}, data) {
    commit('saveField', data)
  },
  creditPurpose({state, commit}, lang) {
    lang = lang || 'ru';

    axios({url: api.adminUrl + api.actions.creditPurpose + `?lang=${lang}`, method: 'GET'})
      .then(resp => {
        let creditPurpose = [];
        resp.data.data.forEach((purpose) => {
          creditPurpose.push({
            value: purpose.id,
            label: purpose.name,
          });
        });

        commit('saveCreditPurposeList', creditPurpose)
      })
      .catch(err => {
        console.log(err)
      })

  },
  workPeriod({state, commit}, lang) {
    lang = lang || 'ru';

    axios({url: api.adminUrl + api.actions.workPeriod + `?lang=${lang}`, method: 'GET'})
      .then(resp => {
        let workPeriods = [];
        resp.data.data.forEach((period) => {
          workPeriods.push({
            value: period.id,
            label: period.name,
          });
        });

        commit('savePeriodList', workPeriods)
      })
      .catch(err => {
        console.log(err)
      })

  },
  cities({state, commit}, lang) {
    lang = lang || 'ru';

    axios({url: api.adminUrl + api.actions.city + `?lang=${lang}`, method: 'GET'})
      .then(resp => {
        let cities = [];
        resp.data.data.forEach((city) => {
          cities.push({
            value: city.id,
            label: city.name,
          });
        });

        commit('saveCitiesList', cities)
      })
      .catch(err => {
        console.log(err)
      })

  },
  employments({state, commit}, lang) {
    lang = lang || 'ru';

    axios({url: api.adminUrl + api.actions.employment + `?lang=${lang}`, method: 'GET'})
      .then(resp => {
        let employments = [];
        resp.data.data.forEach((employment) => {
          employments.push({
            value: employment.id,
            label: employment.name,
          });
        });

        commit('saveEmploymentsList', employments)
      })
      .catch(err => {
        console.log(err)
      })
  },
  registration({state, commit}, password) {
    const data = {
      phone: parseInt(findVal(state.data, 'phone').replace(/\D+/g, '')),
      first_name: findVal(state.data, 'firstName'),
      last_name: findVal(state.data, 'lastName'),
      middle_name: findVal(state.data, 'middleName'),
      email: findVal(state.data, 'email'),
      sex: 'male' === findVal(state.data, 'gender') ? 0 : 1,
      birthday: findVal(state.data, 'birthday').replace(/\//g, '.'),
      password: password.password,
      credit_amount: state.amount,
      city_id: findVal(state.data, 'city'),
      employment_id: findVal(state.data, 'employment'),
      work_period_id: findVal(state.data, 'workPeriod'),
      // credit_purpose_id: ,
    };

    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.update, data: data, method: 'PUT'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  totalCheck({state, commit}, totalCheck) {
    commit('saveTotalCheck', totalCheck)
  },
  addCard({state, commit}) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.card.add, method: 'POST'})
        .then(resp => {
          if (resp.data.url) {
            window.location.href = resp.data.url;
            // window.open(resp.data.url);
          }
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  sendMail({state, commit}) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.mail.send, method: 'POST'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  checkMail({state, commit}, code) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.mail.check, data: code, method: 'POST'})
        .then(resp => {
          console.log(resp)
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  sendPhoneCheckCode({state, commit}) {
    const data = {
      phone: parseInt(findVal(state.data, 'phone').replace(/\D+/g, '')),
      type: 'sms',
    };

    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.phone.send, data, method: 'POST'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  checkPhone({state, commit}, code) {
    const data = {
      phone: parseInt(findVal(state.data, 'phone').replace(/\D+/g, '')),
      code: code,
    };

    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.phone.check, data, method: 'POST'})
        .then(resp => {
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
};

const mutations = {
  setRegistrationStatus(state, isStarted) {
    state.isStarted = isStarted
  },
  setCurrentStep(state, step) {
    state.currentStep = step
  },
  saveData(state, data) {
    state.data[state.currentStep] = data;
  },
  saveField(state, data) {
    if (typeof state.data[state.currentStep] === 'undefined') {
      Vue.set(state.data, state.currentStep, {});
    }

    Vue.set(state.data[state.currentStep], data.field, data.data);
  },
  savePeriodList(state, workPeriod) {
    state.workPeriod = workPeriod;
  },
  saveCreditPurposeList(state, creditPurposeList) {
    state.creditPurpose = creditPurposeList;
  },
  saveCitiesList(state, cities) {
    state.cities = cities;
  },
  saveEmploymentsList(state, employments) {
    state.employments = employments;
  },
  saveTotalCheck(state, totalCheck) {
    state.totalCheck = totalCheck;
  },
  setAmount(state, amount) {
    state.amount = amount;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
