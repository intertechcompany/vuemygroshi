import api from "../../api";
import axios from "axios";

const state = () => ({
  promocodes: [],
});

// getters
const getters = {
  promocodes: state => state.promocodes,
  count: state => state.promocodes.length,
};

const actions = {
  getPromocode({commit}){
    return new Promise((resolve, reject) => {

      axios.get(api.adminUrl + api.actions.promocode.list).then(resp => {
        if(resp.data.success) {
          const promocodes = resp.data.data
          commit('promocodesSuccess', promocodes)
          resolve(resp)
        }
        reject(resp.data.message)
      }).catch(err => {
          reject(err)
        })

    })
  },
};

const mutations = {
  promocodesSuccess(state, promocodes) {
    state.promocodes = promocodes
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
