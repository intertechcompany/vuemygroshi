import api from "../../api";
import axios from "axios";

const state = () => ({
  loadedOffers: [],
  filters: {
    Microloan: {
      minAmount: 1,
      maxAmount: 120000,
      minTerm: 1,
      maxTerm: 15,
      rate: 0,
    },
    Cashloan: {
      minAmount: 1,
      maxAmount: 1000000,
      minTerm: 1,
      maxTerm: 182,
      rate: 0,
    },
    Creditcard: {
      minAmount: 1,
      maxAmount: 500000,
      minTerm: 1,
      maxTerm: 60,
      rate: 0,
    },
  },
  filteredMicroloan: {},
  filteredCashloan: {},
  filteredCreditcard: {},

  offersMicroloan: {},
  offersCashloan: {},
  offersCreditcard: {},

  countMicroloan: 0,
  countCashloan: 0,
  countCreditcard: 0,

  timeLimit: '',
  timeLeft: '',
  expired: false,
});

const getters = {
  offers: state => state.loadedOffers,
  count: state => state.loadedOffers.length,
  offersMicroloan: state => {
    return state.offersMicroloan;
  },
  offersCashloan: state => {
    return state.offersCashloan;
  },
  offersCreditcard: state => {
    return state.offersCreditcard;
  },

  filters: state => {
    return state.filters;
  },
  filteredData: state => name => {
    return state[`filtered${name}`];
  },

  countMicroloan: state => {
    return state.countMicroloan;
  },
  countCashloan: state  => {
    return state.countCashloan;
  },
  countCreditcard: state => {
    return state.countCreditcard;
  },

  timeLimit: state => {
    return state.timeLimit;
  },
  timeLeft: state => {
    return state.timeLeft;
  },
  expired: state => {
    return state.expired;
  },
};

const actions = {
  offers({state, commit}) {
    return new Promise((resolve, reject) => {
      axios.get(api.adminUrl + api.actions.offer).then(resp => {
        if (resp.data.success) {
          const offers = resp.data.data
          commit('saveLoadedOffers', offers)
          resolve(resp)
        }
        reject(resp.data.message)
      })
        .catch(err => {
          reject(err)
        })

    })
  },
  filters({state, commit}, data) {
    if (state.loadedOffers.length) {
      const offersByType = state.loadedOffers.filter(offer => {
        return offer.category === data.category;
      });

      if (offersByType.length) {
        let minAmount = offersByType.reduce(function (prev, curr) {
          return parseFloat(prev.limit_min) < parseFloat(curr.limit_min) ? prev : curr;
        });

        let maxAmount = offersByType.reduce(function (prev, curr) {
          return parseFloat(prev.limit_max) > parseFloat(curr.limit_max) ? prev : curr;
        });

        let minTerm = parseInt(offersByType[0].term_min);
        let maxTerm = parseInt(offersByType[0].term_max);

        let rates = [];

        for (let i = 0; i < offersByType.length; i++) {
          let currMin = parseInt(offersByType[i].term_min);
          let currMax = parseInt(offersByType[i].term_max);

          if (offersByType[i].term_unit === 'месяцев') {
            currMin = parseInt(offersByType[i].term_min * 30);
            currMax = parseInt(offersByType[i].term_max * 30);
          }
          if ((parseInt(minTerm) > parseInt(currMin))) {
            minTerm = currMin;
          }
          if ((parseInt(maxTerm) < parseInt(currMax))) {
            maxTerm = currMax;
          }

          if (offersByType[i].category === 'bank') {
            rates.push(parseFloat(offersByType[i].rate));
          }
        }

        let median = null;
        if (rates.length) {
          rates.sort(function (a, b) {
            return a - b;
          });


          let lowMiddle = Math.floor((rates.length - 1) / 2);
          let highMiddle = Math.ceil((rates.length - 1) / 2);
          median = (rates[lowMiddle] + rates[highMiddle]) / 2;
        }

        let resp = {
          type: data.name,
          data: {
            minAmount: minAmount.limit_min || 1,
            maxAmount: maxAmount.limit_max || 10000,
            minTerm: minTerm || 1,
            maxTerm: maxTerm || 30,
            rate: median || 0,
          }
        };

        commit('saveOfferByType', resp)
      }
    }
  },
  offersMicroloan({state, commit}) {
    const data = {
      name: 'Microloan',
      type: 'mfo'
    };
    commit('setOffers', data);
  },
  offersCashloan({state, commit}) {
    const data = {
      name: 'Cashloan',
      type: 'bank'
    };
    commit('setOffers',data);
  },
  offersCreditcard({state, commit}) {
    const data = {
      name: 'Creditcard',
      type: 'bank'
    };

    commit('setOffers', data);
  },
  setFiltered({state, commit}, data) {
    commit('setFiltered', data);
  },

  setTimeLimit({commit}) {
    let timeLimit = new Date();
    timeLimit.setTime(new Date().getTime() + (180 * 60 * 1000));
    commit('setTimeLimit', timeLimit)
  },
  timeLeft({state, commit}) {
    let minutes = '00';
    let seconds = '00';
    let x = setInterval(function () {
      let countDownDate = new Date(state.timeLimit),
        now = new Date(),
        diff = countDownDate - now,
        thours = Math.floor((diff % 86400000) / 3600000),
        tminutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60)),
        tseconds = Math.floor((diff % (1000 * 60)) / 1000)
      ;
      minutes = (((thours * 60) + tminutes) < 10) ? '0' + tminutes : ((thours * 60) + tminutes);
      seconds = (tseconds < 10) ? '0' + tseconds : tseconds;

      commit('setTimeLeft', {
        minutes,
        seconds,
      })

      if (diff < 0) {
        clearInterval(x);
        commit('setExpired', true)
      }
    }, 1000);
  },
  removeTimeLimit({commit}) {
    commit('removeTimeLimit')
  },
  setExpired({commit}, expired) {
    commit('setExpired', expired)
  },
};

const mutations = {
  saveLoadedOffers(state, offers) {
    state.loadedOffers = offers;
  },
  saveOfferByType(state, data) {
    state.filters[data.type] = data.data;
  },
  setFiltered(state, data) {
    let category = 'mfo';
    if('Microloan' !== data.name) {
      category = 'bank';
    }

    state[`filtered${data.type}`] = data.data;
    let offers = state.loadedOffers.filter(offer => {
      return (
        (offer.category.toString() === category.toString()) &&
        (parseInt(state.filteredMicroloan.term) <= parseInt(offer.term_max)) &&
        (parseInt(state.filteredMicroloan.term) >= parseInt(offer.term_min)) &&
        (parseFloat(state.filteredMicroloan.amount) <= parseFloat(offer.limit_max)) &&
        (parseFloat(state.filteredMicroloan.amount) >= parseFloat(offer.limit_min))
      );
    });

    state[`count${data.type}`] = offers.length || 0;
  },

  setOffers(state, data) {
    state[`offers${data.name}`] = state.loadedOffers.filter(offer => {
       return offer.category === data.type && parseInt(offer.term_max) >= parseInt(state.filteredMicroloan.term) && parseInt(offer.term_min) <= parseInt(state.filteredMicroloan.term) && parseFloat(offer.limit_max) >= parseFloat(state.filteredMicroloan.amount) && parseFloat(offer.limit_min) <= parseFloat(state.filteredMicroloan.amount);
    });
  },

  setTimeLimit(state, timeLimit) {
    state.timeLimit = timeLimit
  },
  removeTimeLimit(state) {
    state.timeLimit = ''
  },
  setTimeLeft(state, timeLeft) {
    state.timeLeft = timeLeft
  },
  setExpired(state, expired) {
    state.expired = expired
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
