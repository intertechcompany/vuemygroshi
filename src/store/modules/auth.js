import api from "../../api";
import axios from "axios";

const state = () => ({
  status: '',
  token: '',
  user: {},
});

// getters
const getters = {
  isLoggedIn: state => !!state.token,
  authStatus: state => state.status,
  token: state => state.token,
  user: state => state.user,
};

const actions = {
  login({commit, dispatch}, user) {
    user.phone =  user.phone.replace(/\D+/g, '');

    return new Promise((resolve, reject) => {
      commit('authRequest')
      axios({url: api.adminUrl + api.actions.login, data: user, method: 'POST'})
        .then(resp => {
          const token = resp.data.access_token
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
          commit('authSuccess', token)
          dispatch('getUser')
          resolve(resp)
        })
        .catch(err => {
          commit('authError')
          reject(err)
        })
    })
  },
  logout({commit}) {
    return new Promise((resolve, reject) => {
      commit('logout')
      delete axios.defaults.headers.common['Authorization']
      resolve()
    })
  },
  deleteUser({commit}) {
    return new Promise((resolve, reject) => {
      commit('authRequest')
      axios({url: api.adminUrl + api.actions.delete, method: 'DELETE'})
        .then(resp => {
          if(resp.data.status) {
            commit('logout')
            delete axios.defaults.headers.common['Authorization']
          }
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },

  getUser({commit}) {
    return new Promise((resolve, reject) => {
      axios({url: api.adminUrl + api.actions.me, method: 'POST'})
        .then(resp => {
          commit('authUser', resp.data)
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  },
  setAuthToken({commit, dispatch}, token) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    commit('authSuccess', token)
    dispatch('getUser')
  },
};

const mutations = {
  authRequest(state) {
    state.status = 'loading'
  },
  authSuccess(state, token) {
    state.status = 'success'
    state.token = token
  },
  authError(state) {
    state.status = 'error'
  },
  logout(state) {
    state.status = ''
    state.token = ''
    state.user = {}
  },
  authUser(state, data) {
    state.user = data
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
