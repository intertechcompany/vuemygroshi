import api from "../../api";
import axios from "axios";

const state = () => ({
    status: '',
});

// getters
const getters = {
    status: state => state.status,
};

const actions = {
    sendData({commit}, data){
        return new Promise((resolve, reject) => {
            axios({url: api.adminUrl + api.actions.feedback, data: data, method: 'POST' })
                .then(resp => {
                    commit('success')
                    resolve(resp)
                })
                .catch(err => {
                    commit('error')
                    reject(err)
                })
        })
    },
};

const mutations = {
    success(state) {
        state.status = 'success'
    },
    error(state) {
        state.status = 'error'
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
