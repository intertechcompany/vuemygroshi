import Vue from 'vue'
import {MLInstaller, MLCreate, MLanguage} from 'vue-multilanguage'
import mlRU from './ru'
import mlUA from './ua'

Vue.use(MLInstaller);

export default new MLCreate({
  initial: 'ru',
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('ru').create(
      mlRU
    ),
    new MLanguage('ua').create(
      mlUA
    ),
  ]
});
