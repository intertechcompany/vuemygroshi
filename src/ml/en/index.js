const mlEN = {
  common: {
    oneDay: 'day',
    severalDays: 'days',
    days: 'days',
    currency: '₸',
    btn: {
      getMoney: 'Get money',
      pickUpOffer: 'Pick up an offer',
      auth: 'Sign In',
      authOrRegistration: 'Login / Registration',
      next: 'Next',
      back: 'Back',
      generatePassword: 'Generate Password',
      registration: 'Registration',
      confirmMail: 'To mail!',
      changeMail: 'Change mail',
      change: 'Change',
      continue: 'Continue',
      removeCard: 'Cancel premium subscription',
      activate: 'Activate',
      send: 'Send',
      later: 'Later',
      get: 'Get',
      copy: 'Copy',
      showResults: 'Show Results',
      more: 'More',
      sendCode: 'Send Code',
      confirmCode: 'Confirm Code',
      reSendCode: 'Resend Code',
      logout: 'Logout',
    },
    breadcrumbs: {
      home: 'Home',
      promocodes: 'Promo Codes',
      registration: 'Registration',
      profile: 'Profile',
      password: 'Password Change',
    },
    email: 'Email',
    genders: {
      male: 'Male',
      female: 'Female',
    },
    step: 'Step',
    of: 'of',
    sum: 'Amount',
  },
  appName: 'MyTenge',
  copyright: '{year}, All rights reserved',
  navLinks: {
    microLoan: 'Micro Loan',
    cashLoan: 'Cash Loan',
    creditCard: 'Credit Card',
    profile: 'My Profile',
    suggestions: 'My Suggestions',
    premium: 'Premium account',
    promotionalCode: 'Promotional Codes',
    logout: 'Logout',
  },
  menu: {
    signIn: 'Sign in',
    continueRegistration: 'Continue Registration',
    registration: 'Registration',

    mobile: {
      signIn: 'Sign in',
      registration: 'Registration',
      support: 'Technical support',

      placeholders: {
        signIn: 'Login',
        password: 'Password',
      },
    }
  },
  footer: {
    contacts: 'Contacts',
    email: 'support@my-tenge.com.ua',
    addressLabel: 'Postal and actual address:',
    address: '350062, Krasnodar, <br> st. Kazbekskaya, 12',
    navigations: 'Navigations',
    documents: 'Documents',
    rates: 'Rates',
    unsubscribe: 'Unsubscribe',
    account: 'Personal Area',
    microLoan: 'Micro Loan',
    credits: 'Cash loans',
    locations: 'Where do we work',
  },
  home: {
    tabs: {
      microloan: 'Microloan',
      credit: 'Credit',
      creditCard: 'Credit card',
      labels: {
        microloan: 'MyTenge - instant microloan on a card online',
        credit: 'MyTenge - cash loan quick and easy',
        creditCard: 'MyTenge - Your credit card in 5 minutes',
      },
      filter: {
        desiredAmount: 'Desired Amount:',
        term: 'Term:',
        loanConditions: 'Loan Conditions',
        creditConditions: 'Credit Conditions',
        loanAmount: 'Loan amount:',
        returnTo: 'Return To:',
        rate: 'Rate:',
      },
    },
    programs: {
      title: 'We would like to make your life easier',
      subtitle: 'Our special programs help you find money for everything you need.',
      items: {
        1: {
          label: 'For any purpose',
          descriptions: 'So that nothing prevents you from making the right decision when you need it',
        },
        2: {
          label: 'On a trip',
          descriptions: 'Helps you get more vivid experiences wherever you go',
        },
        3: {
          label: 'To pay for services',
          descriptions: 'To pay receipts for utilities, close accounts from banks, from individuals and organizations',
        },
        4: {
          label: 'For gifts to loved ones',
          descriptions: 'Enables maximum attention to those you truly love.',
        },
      },
    },
    why: {
      title: 'Why MyTenge:',
      items: {
        1: {
          label: 'approved applications',
          descriptions: 'Highest approval percentage. Approving 9 out of 10 applications',
        },
        2: {
          label: '+3 days without penalties',
          descriptions: 'Forget about 3 days late fines',
        },
        3: {
          label: 'no documents',
          descriptions: 'No more documents needed',
        },
        4: {
          label: 'no calls',
          descriptions: 'We do not meddle with you with intrusive calls',
        },
      },
    },
    safety: {
      title: 'We care about your safety',
      subtitle: 'All operations on the site are protected',
      items: {
        1: {
          description: 'We honestly talk about the terms of the loan - no hidden fees and additional commissions',
        },
        2: {
          description: 'We observe encrypted transmission of your personal data using a secure SSL protocol',
        },
        3: {
          description: 'We honestly talk about the terms of the loan - no hidden fees and additional fees',
        },
      },
    },
    faq: {
      title: 'It\'s very simple',
      subtitle: 'How to get a loan online:',
      items: {
        1: {
          title: '1. Specify the amount and term ',
          description: 'Enter the amount and term of your loan on the main page',
        },
        2: {
          title: '2. Fill out the application ',
          description: 'The process will take no more than 10 minutes on the first call. When you repeat 2 minutes',
        },
        3: {
          title: '3. Choosing a bank or MFO ',
          description: 'Our site will provide you with the best conditions. Choose the bank you like ',
        },
        4: {
          title: '4. Get your money ',
          description: 'Wait for the application to be processed, and you will be able to receive your money',
        },
      },
    },
    questions: {
      title: 'Questions and Answers',
      subtitle: 'Get answers to the most frequently asked questions:',
      items: {
        1: {
          title: '1. How much money will I get here? ',
          text: 'For now, the maximum loan amount that you can arrange through MyGroshi is UAH 30,000. However, it is possible that the limit will soon be increased.',
        },
        2: {
          title: '2. How quickly will I receive the money? ',
          text: 'For now, the maximum loan amount that you can arrange through MyGroshi is UAH 30,000. However, it is possible that the limit will soon be increased.',
        },
        3: {
          title: '3. Probably you need a lot of paperwork? ',
          text: 'For now, the maximum loan amount that you can arrange through MyGroshi is UAH 30,000. However, it is possible that the limit will soon be increased.',
        },
        4: {
          title: '4. And if my credit history is not perfect, will they give me a loan? ',
          text: 'For now, the maximum loan amount that you can arrange through MyGroshi is UAH 30,000. However, it is possible that the limit will soon be increased.',
        },
        5: {
          title: '5. I don\'t want to go anywhere. Can I get money on the card? ',
          text: 'For now, the maximum loan amount that you can arrange through MyGroshi is UAH 30,000. However, it is possible that the limit will soon be increased.',
        },
        6: {
          title: '6. Can I repay the loan early? ',
          text: 'For now, the maximum loan amount that you can arrange through MyGroshi is UAH 30,000. However, it is possible that the limit will soon be increased.',
        },
      },
    },
    importantInformation: {
      title: 'Important information',
      items: {
        1: {
          title: 'Maturities and Interest Rates?',
          text: 'Maturities and interest rates',
        },
        2: {
          title: 'Example of calculating an online loan',
          text: 'Example of calculating an online loan',
        },
        3: {
          title: 'What happens if loan funds are not paid',
          text: 'What happens if loan funds are not paid',
        },
      },
    },
    consumerInformation: {
      title: 'Consumer Information',
      text: 'Services on the project "MyTenge" are provided by IE SIMOV NA, INN 615018734300, OGRNIP 320237500090117 If you have any questions about the work of the service, you can read the section Questions and Answers, or ask your question to our support service <br> <br> The project selects financial products for clients, acting as an intermediary between a client wishing to take out a loan and a licensed financial institution. The my-tenge.info project is not a financial institution, bank or lender, and is not responsible for any loan agreements or conditions entered into. Calculations are approximate; check the final conditions on the credit institution\'s website. To place an application, you need to familiarize yourself with the rules of the service and fill out a form. The site uses cookies to provide services. You can configure the conditions for storing or accessing cookies in your browser. The service is intended for people over 18 years old. The cost of services is regulated by the current tariffs. If the service has ceased to be relevant for you (you have received a loan or no longer need loans), you can independently unsubscribe from the service at any time - cancel your subscription <br> <br> Attention! The service of processing the application is paid and is 1932 tenge, the write-off is made in the currency of the Russian Federation (rubles). Service further on the site - "Premium account" To deactivate the subscription, remove the cards in the {account} site. ',
      personalArea: 'personal account',
    },
  },
  registration: {
    label: 'Application for a microloan',
    approval: 'Probability of approval',
    loansLeft: 'Loans left',
    preloaderTitle: 'Please wait',
    preloaderDescription: 'Registration in progress',
    firstStep: {
      lastName: 'Last Name',
      firstName: 'Name',
      middleName: 'Middle Name',
      gender: 'Gender',
      birthday: 'Birthday',
      city: 'City',

      placeholders: {
        lastName: 'Enter last name',
        firstName: 'Enter first name',
        middleName: 'Enter middle name',
        gender: 'Select gender',
        birthday: 'dd / mm / yyyy',
        city: 'Select a city',
      },
    },
    secondStep: {
      email: 'E-Mail address',
      phone: 'Phone number',
      employment: 'Employment',
      workPeriod: 'Work period',
      probability: 'Get <span class="text-green text-semi-bold">+{probability}%</span> to the likelihood of getting a loan',

      placeholders: {
        email: 'Enter E-Mail',
        phone: 'Enter Phone',
        employment: 'Select a job placement',
        workPeriod: 'Select work period',
      }
    },
    thirdStep: {
      password: 'Password',
      repeatPassword: 'Repeat password',

      placeholders: {
        password: 'Create a password',
        repeatPassword: 'Repeat password',
      },
    },
    confirmMailStep: {
      email: 'For identification, please confirm your Email address. \ n' +
        'The letter was sent to the mail:',
      info: '* If you have not received your email, please check your <span class="text-green">Spam folder</span>',
      code: 'Enter confirmation code:',
      step: {
        description: 'Almost done!',
        code: '****',
      },

      placeholders: {
        email: 'example@example.com',
      },
    },
    confirmMailFinishStep: {
      description: 'Mail <span class = "text-black text-semi-bold">{email}</span> confirmed successfully!',
    },
    checkDataStep: {
      title: '{name}{comma} your data <div class="desktop-br"></div>is being checked',
      dataVerified: 'Data verified:',
      description: {
        top: 'We are currently analyzing your credit score against our scoring model using algorithms.',
        bottom: 'It won\'t take more than <span class="text-green"> 1 minute </span>. Please do not close the page. The results will be sent to you by email and SMS.',
      },
      items: {
        name: 'Checking full name',
        birthday: 'Checking the date of birth',
        work: 'Checking Employment',
        email: 'Checking E-Mail',
        phone: 'Checking phone number',
      },
    },
    checkDataFinishStep: {
      label: {
        top: '{name}{comma} thanks for waiting',
        bottom: 'Validation of your data is complete',
      },
      description: 'The results of the data verification were sent to your email and SMS notification to your phone number.',
    },
    addCardStep: {
      label: {
        top: '<span class = "text-green"> Attention! </span> There is not enough bank card',
        bottom: 'Bank details are missing',
      },
      description: 'In order to receive a loan, you need to link a card, 1 hryvnia will be debited from you for services of selection of offers. \ n' +
        'The card must have a positive balance and the presence of deposit / withdrawal operations for the last 30 days.',
      agreement: 'I agree with',
      agreementPolicy: 'public offer and subscription; I consent to the processing of personal data and receipt of advertising materials. </span> ',
      agreementNotifed: 'I have been notified of the debiting of 1 hryvnia towards the cost of the service when binding the card. I instruct the operator to apply <span class = "text-blue"> recurring payments. </span> ',
      safety: {
        label: 'We care about safety',
        content: {
          top: 'Security of payments is carried out with the help of BANK-EQUAYER, which operates on the basis of modern protocols and technologies developed by international payment systems VISA International and MasterCard International (3D Secure, USAF). The security of the transmitted information is ensured using modern security protocols on the Internet (SSL / TLS) The transmission of the confidential data of the Cardholder (card details, registration data, etc.) is carried out over closed data transmission networks certified by the international banking security standards PSI DSS Level 1 . ',
          bottom: 'We remind you that the service provides paid services - the cost of activating a premium account is 1 (one) hryvnia (one-time payment) plus 119 (one hundred and nineteen) hryvnia every month of using the service - the service is provided in accordance with all site rules and Ukrainian legislation. Service activation does not give a 100% guarantee of obtaining a loan. A premium account allows you to send applications in unlimited quantities and increases the chances of getting a loan for topics. If the service has ceased to be relevant to you (you have received a loan or do not need loans anymore), you can unsubscribe from the service yourself at any time. ',
        },
      },
    },
    addCardFinishStep: {
      label: {
        top: 'Enter your bank card details',
        bottom: '<span class = "text-green text-semi-bold"> + 20% </span> to be approved for filling in the card data',
      },
      placeholders: {
        card: {
          number: 'Card number',
          valid: 'mm / yy',
          month: 'Month',
          year: 'Year',
          cvv: 'CVV',
        },
      },
      description: {
        date: 'Enter month and year',
        cvv: '3 digits on the back <br> side of the card',
      },
      info: 'For identification purposes, an amount of 0.5 KZT will be debited from your card and returned. Then, for the provision of services, within a day you will be charged a payment in the amount of 133 The cost of the service is 133 KZT. / month '
    },
    cardPopUp: {
      success: {
        description: {
          main: 'Your map has been successfully added!',
          sub: '',
        },
        btn: 'Continue',
      },
      error: {
        description: {
          main: 'Something went wrong!',
          sub: 'Map has not been added! Check card balance and try again ',
        },
        btn: 'Back',
      },
    },
    addCardConfirm: {
      label: 'You have been sent an SMS code from {bankName}',
      description: 'You have been sent an SMS-code from {bankName}, enter it to confirm:',
      placeholders: {
        code: 'Code from SMS',
      },
    },
    confirmPhoneStep: {
      phone: 'Phone number',
      code: 'Verification code',
      placeholders: {
        phone: 'Enter phone number',
        code: 'Enter verification code',
      },
    },
  },
  promocodes: {
    title: 'My promo codes',
    subtitle: 'Special offers for you',
    popup: {
      title: 'Promo code <span class="text-green"> {mfoName} </span>',
      description: 'Using the promo code, you get a discount on services and an interest rate when </br> a loan in <span class="text-green">{mfoName}</span>',
    }
  },
  profile: {
    personal: 'Personal data',
    save: 'Save',
    edit: 'Edit',
    offers: 'My offers',
    premiumAccount: 'Premium Account',
    promoCodes: 'Promo Codes',
    settings: 'Profile settings',
    changePass: 'Change password',
    delete: 'Delete profile',
    premium: {
      title: 'PREMIUM ACTIVATED!',
      desc: 'Premium status active until:',
      renew: 'Renew',
    },
    cards: {
      title: 'Your cards',
      submit: 'Submit',
      needSubmit: 'Confirmation Required',
      delete: 'Delete',
      ready: 'Ready',
      add: 'Add map',
    },
    user: {
      fio: 'Full name',
      birthday: 'Date of birth:',
      phone: 'Mobile phone:',
      email: 'E-mail address:',
      city: 'City:',
      country: 'Country of residence:',
      cards: 'Your cards',
      inn: 'TIN',
      employment: 'Employment',
      workPeriod: 'Work period',
      creditPurpose: 'Credit Purpose',
      creditAmount: 'Credit Amount',
    },
    remove: {
      label: 'Will all your data be removed from the system',
      title: '',
      btn: {
        confirm: 'Yes, delete',
        cancel: 'Cancel',
      },
      card: {
        label: 'Your card will be removed',
        title: '',
      },
    },
  },
  blocks: {
    premium: {
      label: 'PREMIUM!',
      description: 'Remove the restriction and submit all approved applications. </br> Get the opportunity to view <span class = "text-black text-semi-bold"> chance of approval </span> and <span class = "text-black text-semi-bold"> bank ratings </span>',
      button: {
        label: 'Get Premium',
        description: 'the service cost is {amount} {currency}',
      },
    },
    warning: {
      label: 'IMPORTANT!',
      description: 'Your account is subject to a restriction: </br> <span class = "text-black text-semi-bold"> 1 request per 15 minutes. </span>',
    },
    timer: {
      description: 'The next order will be available after the time expires above',
      button: {
        label: 'Get Premium',
        description: 'the service cost is {amount} {currency}',
      },
    },
  },
  premium: {
    questions: {
      title: 'Frequently Asked Questions',
    },
    faq: {
      label: {
        top: 'What is it',
        bottom: 'premium?',
      },
      items: {
        1: {
          text: 'Rating of reliability of credit institutions',
        },
        2: {
          text: 'Probability of approval for your application for each proposal',
        },
        3: {
          text: 'No time limit',
        },
        4: {
          text: 'No time limit',
        },
        5: {
          text: 'Send applications to all approved credit institutions',
        },
        6: {
          text: 'New offers every day',
        },
        7: {
          text: 'SMS informing about the status of your applications',
        },
      },
      title: 'PREMIUM!',
      description: 'Premium status active! Thanks!',
      more: 'more about account',
    },
    description: {
      label: 'Remove the restriction and submit all approved entries. Get the opportunity to view the chance of approvals and ratings of banks! ',
      premium: 'Buy premium',
      sublabel: 'the service cost is {amount} {currency}',
    },
    importantInformation: {
      items: {
        1: {
          title: 'How much does the premium cost?',
          text: 'How much does the premium cost?',
        },
        2: {
          title: 'How long is the premium valid?',
          text: 'How long is the primer valid?',
        },
        3: {
          title: 'Can I unsubscribe from the premium?',
          text: 'Can I opt out of premium?',
        },
        4: {
          title: 'Is Premium Required?',
          text: 'Is premium required?',
        },
      },
    },
  },
  passwordUpdate: {
    title: 'Change Password',
    newPassword: 'Change password:',
    repeatNewPassword: 'Repeat password:',
    valid: 'Passwords match',
    updateBtn: 'Update password',
  },
  unsubscribe: {
    label: 'Unsubscribe from paid services',
    sublabel: 'Enter the first 6 and last 4 digits of the card number you want to remove from your account',
    form: {
      label: 'Map data',
      btn: 'Unsubscribe',
    },
    modal: {
      label: 'Are you sure you want to unsubscribe?',
      title: '',
      btn: {
        confirm: 'Yes',
        cancel: 'Cancel',
      },
    }
  },
}

export default mlEN;
