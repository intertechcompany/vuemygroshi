const api = {
  siteUrl: 'https://my-tenge.info',
  // adminUrl: 'production' === process.env.NODE_ENV ? 'https://admin.my-tenge.info' : 'https://admin-ua.my-tenge.info',
  adminUrl: 'https://admin-ua.my-tenge.info',

  actions: {
    offer: '/api/offer',
    offerUse: '/api/offer/use',
    me: '/api/auth/me',
    delete: '/api/auth/delete',
    refresh: '/api/auth/refresh',
    login: '/api/auth/login',
    feedback: '/api/feedback',
    workPeriod: '/api/list/work_period',
    creditPurpose: '/api/list/credit_purpose',
    city: '/api/list/city',
    employment: '/api/list/employment',
    registration: '/api/auth/registration',
    update: '/api/auth/update',
    card: {
      add: '/api/payment/add_card',
      remove: '/api/payment/del_card',
    },
    mail: {
      send: '/api/auth/email_send_code',
      check: '/api/auth/email_check_code',
    },
    phone: {
      send: '/api/auth/phone_send_code',
      check: '/api/auth/phone_check_code',
    },
    promocode: {
      list: '/api/promocode',
    },
    userUpdate: '/api/auth/update',
    settings: '/api/settings',
    premium: {
      start: '/api/payment/subscription_start',
      stop: '/api/payment/subscription_stop',
    },
  },
};

export default api
